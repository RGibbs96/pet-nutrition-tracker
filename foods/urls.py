from django.urls import path
from .views import foods_list

urlpatterns = [
    path("foods", foods_list, name="foods_list")
]
