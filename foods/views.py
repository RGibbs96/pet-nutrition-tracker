from django.shortcuts import render
from .models import Food

def foods_list(request):
    foods = Food.objects.all()
    context = {
        "foods": foods,
    }
    return render(request,"foods/food_list.html", context)
